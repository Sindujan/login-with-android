package com.example.login_connect;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapterMeds extends RecyclerView.Adapter<RecyclerViewAdapterMeds.MyViewHolder> {


    private Context mContext;
    private List<Model_Meds> mData;
  //  RequestOptions option;

    public RecyclerViewAdapterMeds(Context mContext, List<Model_Meds> mData) {
        this.mContext = mContext;
        this.mData = mData;
        //Request for Glide
      //  option = new RequestOptions().centerCrop().placeholder(R.drawable.loading_shape).error(R.drawable.loading_shape);

    }

    @NonNull
    @Override
    public RecyclerViewAdapterMeds.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view=inflater.inflate(R.layout.card_user,viewGroup,false);


        return new RecyclerViewAdapterMeds.MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterMeds.MyViewHolder myViewHolder, int i) {

        myViewHolder. textView.setText(mData.get(i).getStudentname());
        myViewHolder. textView2.setText(mData.get(i).getDate());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder
    {

       TextView textView2,textView;




        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            textView2 = itemView.findViewById(R.id.editText2);
            textView = itemView.findViewById(R.id.editText);


        }



    }


}