package com.example.login_connect;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Jsonplaceholder {

    @POST("signin")
    Call<Login_Response> login(@Body Login_Request login_request);


    @GET("/badges/add")
    Call<ResponseBody> addbatch(@Query("studentname") String studentname,
                                @Query("batch") String batch,
                                @Query("date") String date,
                                @Query("time") String time,
                                @Query("sender") String sender);
}
