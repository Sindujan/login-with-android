package com.example.login_connect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SecondPage extends AppCompatActivity {

    private JsonArrayRequest request;
    private RequestQueue requestQueue;
    private List<Model_Meds> medsList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        medsList = new ArrayList<>();
        setContentView(R.layout.activity_second_page);
    }


    private void jsonrequest(String url) {
        request = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {



                JSONObject jsonObject = null;

                for(int i =0 ; i< response.length() ; i++)
                {
                    try {
                        jsonObject= response.getJSONObject(i);
                        Model_Meds med = new Model_Meds();

                        med.setStudentname(jsonObject.getString("studentname"));
                        med.setDate(jsonObject.getString("date"));
                        med.setTime(jsonObject.getString("time"));
                        med.setFirstaid(jsonObject.getString("firstaid"));
                        med.setIncident(jsonObject.getString("incident"));
                        medsList.add(med);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                setuprecyclerview(medsList);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });


        requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(request);



    }

    private void setuprecyclerview(List<Model_Meds> meds)
    {
        RecyclerViewAdapterMeds myadapter = new RecyclerViewAdapterMeds(getApplicationContext(),meds);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        recyclerView.setAdapter(myadapter);
    }

}
